# SPDX-FileCopyrightText: 2024 Helmholtz Centre for Environmental Research (UFZ)
#
# SPDX-License-Identifier: AGPL-3.0-only

import streamlit as st
import os

SECRETS_FILE_PATH = "/secrets/config.yaml"


def main():
    st.set_page_config(layout='wide')

    st.title("Streamlit on WOMBAT")
    st.write("## Welcome to Streamlit example page at UFZ 🎈🎈")

    st.sidebar.markdown("# Main page 🎈")

    # my_password = os.environ.get('MY_PASSWORD')
    #
    # st.write("### My Password is:")
    # st.write(f"{my_password}")
    # st.write(f"... more text")
    #
    # st.write("### Contents of my mounted file:")
    # st.write(read_contents_from_file_in_mounted_folder())


def read_contents_from_file_in_mounted_folder():
    content = None
    with open(SECRETS_FILE_PATH, 'r') as file:
        # Read the entire content of the file
        content = file.read()
    return content


if __name__ == "__main__":
    main()
