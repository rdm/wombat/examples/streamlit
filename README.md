<!--
SPDX-FileCopyrightText: 2023 Helmholtz Centre for Environmental Research (UFZ)

SPDX-License-Identifier: AGPL-3.0-only
-->

# streamlit@Wombat

![](https://argocd.web-intern-stage.app.ufz.de/api/badge?name=streamlit-example)

Example project with best practises - preparation for scientific apps to run on UFZ Wombat Kubernetes Cluster 

https://streamlit.io/

## Getting started

### Requirements
- docker / docker-compose

## local development

Checkout
```
git clone git@git.ufz.de:rdm/wombat/examples/streamlit.git
```

Adjust `.env`-File
```
cp .env.sample .env
```

BuildnRun
```
docker build -t localhost/streamlit:dev .
docker run --rm -p 8501:8501 -v ./src:/app -v ./src/config.yaml.sample:/secrets/config.yaml localhost/streamlit:dev
```

or
```
docker compose up
```

## Secrets

### Environment Variables
Use environment variables for passing secrets like passwords to your app. Do not commit them into your git-repo!

- use in code via `os.environ.get("NAME_OF_ENV_VAR")`
- declare in Dockerfile `ENV NAME_OF_ENV_VAR default_value`
- use in docker command `docker run -e NAME_OF_ENV_VAR=value` or use an `.env`-File

### Bind Mounts

Use bind-mounts to inject whole files with secrets into your container. Do not commit them into your git-repo!
- use in code via usual file access `open(SECRETS_FILE_PATH, 'r')`
- declare Volume in Dockerfile `VOLUME /secrets`
- use in docker command: `docker run --rm -p 8501:8501 -v ./src:/app -v ./src/config.yaml.sample:/secrets/config.yaml localhost/streamlit:dev`


## How to create and push a new Image to registry

This is done via Gitlab-CI/CD Pipeline. If you push a new Commit to main, the `latest`-Image will be build and pushed to
the projects registry automatically.

If you want to deploy a release to Wombat, you should create a Git-Tag with a certain version number (X.Y.Z) then another
pipeline creates a DockerImage with this name automatically


## FAIR

https://reuse.software/

